<ul class="sidebar-menu">
  <li class="active">
    <a  href="<?php echo site_url('view/home');?>" class="">
      <i class="fa fa-clock-o fa-fw" aria-hidden="true"></i> <span>Dashboard</span>
    </a>
  </li>
  <li>
    <a href="#" onclick="loadContent(base_url + 'view/_article_form'); return false;" class="waves-effect"><i class="fa fa-book fa-fw" aria-hidden="true"></i>Article</a>
  </li>
  <li>
    <a href="#" onclick="loadContent(base_url + 'view/_comment_form'); return false;" class="waves-effect"><i class="fa fa-font fa-fw" aria-hidden="true"></i>Comment</a>
  </li>
  <li>
    <a href="#" onclick="loadContent(base_url + 'view/_user_form'); return false;">
      <i class="fa fa-users"></i>
      <span> User Management</span>
    </a>
  </li>                
  <li>
    <a href="#" onclick="loadContent(base_url + 'view/_setting_form'); return false;" class="waves-effect"><i class="fa fa-gears fa-fw" aria-hidden="true"></i>Setting</a>
  </li>
  
  <li>
    <a href="#" onclick="loadContent('<?php echo site_url('view/_gallery_form');?>')">
      <i class="fa fa-image fa-fw"></i>
      <span>Image</span>
    </a>
  </li>
  <li>
    <a href="#" onclick="loadContent('<?php echo site_url('view/_gallery_video_form');?>')">
      <i class="fa fa-film fa-fw"></i>
      <span>video</span>
    </a>
  </li>
  <li>
    <a href="#" onclick="loadContent('<?php echo site_url('view/_rumahsakit_form');?>')">
      <i class="fa fa-hospital-o fa-fw"></i>
      <span>Rumah Sakit</span>
    </a>
  </li>
  <li>
    <a href="#" onclick="loadContent('<?php echo site_url('view/_dokter_form');?>')">
      <i class="fa fa-user-md fa-fw"></i>
      <span>Dokter</span>
    </a>
  </li>
  <li>
    <a href="#" onclick="loadContent('<?php echo site_url('view/_jadwaldokter_form');?>')">
      <i class="fa fa-tasks fa-fw "></i>
      <span>Jadwal Dokter</span>
    </a>
  </li>
</ul>