<div class="site-about">
  <div class="site-bg lazy" data-src="/img/bg3.jpg">
    <div class="row judul-back">
      <h1 class="title-lokasi-home col-md-5">Artikel</h1>
      <div class="search-lk search-event col-md-3 col-md-push-4 col-xs-12">
        <div class="row">
          <div class="col-md-12">
            <form id="search-keyword">
              <div class="input-group">
                <input type="text" class="form-control select-info-dokter" id="input-keyword" placeholder="Cari kata kunci..." value="" />
                <div class="input-group-btn">
                  <button class="btn btn-default btn-search-keyword" type="submit">
                    <i class="glyphicon glyphicon-search"></i>
                  </button>
                </div>
              </div>
              <!-- <div class="input-group">
                <input type="text" class="form-control select-info-dokter" id="input-keyword" placeholder="Cari kata kunci..." value="" />
                <div class="input-group-btn">
                  <button class="btn btn-default btn-search-keyword" type="submit">
                    <i class="glyphicon glyphicon-search"></i>
                  </button>
                </div>
              </div> -->
            </form>
          </div>
          <!--
                    <div class="col-md-12 col-xs-12 search-top" id="search-artikel">
                        <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                        <span></span>
                        <span class="hidden slug"></span>
                    </div>
-->
        </div>
      </div>
    </div>
    <div class="card card-site card-ha">
      <div class="loading-klinik hidden text-center">
        <h2><span class="glyphicon glyphicon-refresh is-loading"></span> Loading...</h2>
      </div>
      <div class="news-part">
        <div class="">
          <div class="row">
            <div class="row" id="health-article-content">
              <?php
              if ($informasi) {

                foreach ($informasi as $key => $value) {
              ?>
                  <div class="card-health col-md-4" onclick="location.href='<?= base_url(); ?><?= $rs ?>/informasi/<?= $value->link ?>';" itemscope itemtype="https://schema.org/Article">
                    <meta itemprop="headline" content="Artikel" />
                    <meta itemprop="author" content="Rumah Sakit Permata Keluarga" />
                    <meta itemprop="mainEntityOfPage" content="/artikel" />
                    <meta itemprop="dateModified" content="2021-01-25" />
                    <div class="hidden" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                      <meta itemprop="name" content="Rumah Sakit Permata Keluarga" />
                      <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                        <meta itemprop="url" content="img/logo.png" />
                      </div>
                    </div>
                    <div class="overlay-tips">
                      <div class="crop-card">
                        <img class="card-img-top lazy" data-src="<?php echo base_url(); ?>assets/image/article/<?= $value->img ?>" itemprop="image" content="<?php echo base_url(); ?>assets/image/article/<?= $value->img ?>" />
                      </div>
                      <div class="card-health-overlay">
                        <button class="btn btn-white btn-health button-read-more">Baca</button>
                      </div>
                    </div>
                    <div class="card-body card-body-tips">
                      <h4 class="card-title-tips" itemprop="name"><a href="<?= base_url(); ?><?= $rs ?>/informasi/<?= $value->link ?>" itemprop="url"><?= $value->title ?></a></h4>
                      <small class="text-muted" itemprop="datePublished" content="2021-01-25">25 Januari 2021 </small>
                      <p class="card-text card-text-tips hidden-xs hidden-sm" itemprop="text"><?= strip_tags(substr($value->content, 0, 300)) . '....'; ?>
                      </p>
                    </div>
                  </div>

              <?php
                }
              }
              ?>
            </div>
            <p class="fade all-loaded text-center">Semua artikel telah dimuat</p>
            <div class="text-center">

              <div <?= $all ?>>
                <ul class="pagination pagination-lg">
                  <?php
                  foreach ($links as $link) {
                    echo "<li>" . $link . "</li>";
                  }
                  ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script id="health-article-content-template" type="text/template">
    <div class="card-health col-md-4" onclick="SLUG" itemscope itemtype="https://schema.org/Article">
        <meta itemprop="headline" content="Artikel"/>
        <meta itemprop="author" content="Rumah Sakit Permata Keluarga"/>
        <meta itemprop="mainEntityOfPage" content="/artikel"/>
        <meta itemprop="dateModified" content="{{PUREDATE}}"/>
        <div class="hidden" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
            <meta itemprop="name" content="Rumah Sakit Permata Keluarga"/>
            <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                <meta itemprop="url" content="img/logo.png"/>
            </div>
        </div>
        <div class="overlay-tips">
            <div class="crop-card">
                <img class="card-img-top" src="{{IMAGE}}" itemprop="image">
            </div>
            <div class="card-health-overlay">
                <a class="btn btn-white btn-health button-read-more" href="SLUG" itemprop="url">Baca</a >
            </div>
        </div>
        <div class="card-body card-body-tips">
            <h4 class="card-title-tips" itemprop="name"><a href="SLUG">{{TITLE}}</a></h4>
            <small class="text-muted" itemprop="datePublished" content="{{PUREDATE}}">{{DATE}}
            </small>
            <p class="card-text card-text-tips hidden-sm hidden-xs" itemprop="text">{{EXCERPT}}
            </p>
        </div>
    </div>
</script>
  <script type="text/javascript">
    var domain = "";
    var domain_search_article = "/api/search-article";
  </script>
</div>