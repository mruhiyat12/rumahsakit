<?php
$this->load->helper('url');
$rs = $this->uri->segment(1);
$list_rs = $this->frontmodel->get_opt_rs();
$id_rumahsakit = ($this->uri->segment(1)) ?  $this->uri->segment(1) : 'lippo';
switch ($id_rumahsakit) {
  case 'lippo':
    $alamat = '15';
    $tlp = '18';
    $igd = '11';
    $fax = '12';
    $email = '26';
    $id_rs = '1';
    break;
  case 'jababeka':
    $alamat = '16';
    $tlp = '24';
    $igd = '1';
    $fax = '2';
    $email = '27';
    $id_rs = '2';
    break;
  case 'galuhmas':
    $alamat = '17';
    $tlp = '25';
    $email = '28';
    $id_rs = '3';
    break;
}

$alamats = $this->frontmodel->getSingleSettingKontak($alamat, $id_rs);
$tlps = $this->frontmodel->getSingleSettingKontak($tlp, $id_rs);
$igds = $this->frontmodel->getSingleSettingKontak($igd, $id_rs);
$faxs = $this->frontmodel->getSingleSettingKontak($fax, $id_rs);
$emails = $this->frontmodel->getSingleSettingKontak($email, $id_rs);
?>
<div class="rumah-sakit-klinik">
  <div class="row content-rs-klinik">
    <div class="rumah-sakit-klinik">
      <div class="row content-rs-klinik">
        <div class="col-md-7 col-xs-12 img-rs-klinik">
          <?php
          $maps = $this->frontmodel->getSingleSettingMaps($id_rs);
          if ($maps) {
            echo
            "<iframe src='" . $maps[0]->value_set . "' width='740' height='360' frameborder='0' style='border:0' allowfullscreen></iframe>";
          }
          ?>
        </div>
        <div class="col-md-4 col-xs-12 detail-rs-lokasi">


          <h4 class="title-footer-top">Kontak Kami :</h4>
          <?php
          $this->load->helper('url');
          $rs = $this->uri->segment(1);
          $list_rs = $this->frontmodel->get_opt_rs();
          $id_rumahsakit = ($this->uri->segment(1)) ?  $this->uri->segment(1) : 'lippo';
          switch ($id_rumahsakit) {
            case 'lippo':
              $alamat = '15';
              $tlp = '18';
              $igd = '11';
              $fax = '12';
              $email = '26';
              $id_rs = '1';
              break;
            case 'jababeka':
              $alamat = '16';
              $tlp = '24';
              $igd = '1';
              $fax = '2';
              $email = '27';
              $id_rs = '2';
              break;
            case 'galuhmas':
              $alamat = '17';
              $tlp = '25';
              $email = '28';
              $id_rs = '3';
              break;
          }

          $alamats = $this->frontmodel->getSingleSettingKontak($alamat, $id_rs);
          $tlps = $this->frontmodel->getSingleSettingKontak($tlp, $id_rs);
          $igds = $this->frontmodel->getSingleSettingKontak($igd, $id_rs);
          $faxs = $this->frontmodel->getSingleSettingKontak($fax, $id_rs);
          $emails = $this->frontmodel->getSingleSettingKontak($email, $id_rs);
          ?>
          <h3 class="klinik-title blue"><b>Permata Keluarga <?= strtoupper($rs) ?></b></h3>
          <div class="detail-rs-home">
            <div class="row">
              <div class="col-md-1 col-xs-1">
                <span class="glyphicon glyphicon-map-marker blue" aria-hidden="true" style="display: inline;"></span>
              </div>
              <div class="col-md-10 col-xs-10">
                <span><?= $alamats[0]->value_set  ?></span>
              </div>
            </div>
          </div>
          <div class="detail-rs-home">
            <div class="row">
              <div class="col-md-1 col-xs-1">
                <span class="glyphicon glyphicon-earphone blue" aria-hidden="true"></span>
              </div>
              <div class="col-md-10 col-xs-10">
                <span>
                  <p>Informasi :&nbsp;<span style="font-weight: 400;"><?= $tlps[0]->value_set  ?></span></p>
                  <p>IGD :&nbsp;<span style="font-weight: 400;"><?= $igds[0]->value_set ?></span></p>
                  <p>Fax :&nbsp;<span style="font-weight: 400;"><?= $faxs[0]->value_set ?></span></p>
                  <p><span style="font-weight: 400;">Whatsapp : <?= $tlps[0]->value_set ?></span></p>
                </span>
              </div>
            </div>
          </div>
          <div class="detail-rs-home">
            <div class="row">
              <div class="col-md-1 col-xs-1">
                <span class="glyphicon glyphicon-envelope blue" aria-hidden="true"></span>
              </div>
              <div class="col-md-10 col-xs-10">
                <span><?= $emails[0]->value_set ?></span>
              </div>
            </div>
          </div>
          <div class="detail-rs-home">
            <div class="row">
              <div class="col-md-1 col-xs-1">
                <i class="fab fa-facebook-square blue"></i>
              </div>
              <div class="col-md-10 col-xs-10"><span><a href="#" target="_blank">Permata Keluarga <?= strtoupper($rs) ?></a></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--/.container-->
</script>
<script type="text/javascript">
  $(document).ready(function() {
    CKEDITOR.replace('content-input', {
      removeButtons: 'Source,Save,NewPage,Preview,Print,Templates,Cut,Undo,Copy,Redo,Paste,PasteText,PasteFromWord,Find,Replace,SelectAll,Scayt,Form,HiddenField,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,NumberedList,BulletedList,CreateDiv,BidiLtr,BidiRtl,Language,Link,Unlink,Anchor,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Styles,Format,Font,FontSize,TextColor,BGColor,Maximize,ShowBlocks,About,Indent,Outdent,Blockquote,Strike'
    });
  });

  function proccess() {
    setTimeout(function() {
      $.ajax({
        url: base_url + 'front/send_guestbook',
        data: $("#guestbook").serialize() + '&title-input=Guestbook',
        dataType: 'json',
        type: 'POST',
        cache: false,
        success: function(json) {
          if (json.code === 0) {
            if (json.message == '') {
              $("#name-input, #email-input, #phone-input, #content-input").val("");
              alert('Pesan gagal dikirim!');
            } else {
              alert(json.message);
            }
          } else {
            alert(json.message);
            $("#name-input, #email-input, #phone-input, #content-input").val("")
          }
        },
        error: function() {
          alert('Terjadi kesalahan!');
        }
      });
    }, 100);
  }
</script>