<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| FrontEnd Config
|--------------------------------------------------------------------------
*/
$config['app_name']			= 'Permata keluarga Info';
$config['app_logo_mini']	= 'CMS';
$config['app_logo_lg']		= 'Permata Keluarga';
$config['app_version']		= '1.0 Beta';

/*
|--------------------------------------------------------------------------
| BackEnd Config
|--------------------------------------------------------------------------
*/
//$config['sys_private_key'] = 'DfYDFHowqyMnBGRUcPThYP4gNaogWk3k';

/* email config */
// $config['sys_email_sender']			= 'noreply@publicspeakingsbc.com';
// $config['sys_email_sender_name']	= 'Admin publicspeakingsbc.com';
// $config['sys_email_host']			= 'smtp.zoho.com';
// $config['sys_email_port']			= '587';
// $config['sys_email_user']			= 'noreply@publicspeakingsbc.com';
// $config['sys_email_pass']			= 'sbc2017#';

//seting time zone
date_default_timezone_set("Asia/Jakarta");


/* End of file config_app.php */
/* Location: ./application/config/config_app.php */
